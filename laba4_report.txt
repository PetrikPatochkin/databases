Тема роботы: продемонстрировать навыки использования различных конструкций SQL

Выполнение работы:

Список транзакций:
1. Вывод всей информации о каждом гражданинне сортированых по дате рождения.
SELECT citizen.id,citizen.name,citizen.surname,citizen.birthday,
class.name AS class,region.name AS region FROM citizen 
LEFT JOIN class ON (class.id=citizen.id_class)
LEFT JOIN region ON (region.id=citizen.id_region)
ORDER BY citizen.birthday ASC;

2.Общее количество произведенных товаров в королевстве в количестве больше 100.
SELECT production.id_good,good.name,SUM(production.amount) FROM production
INNER JOIN good ON (good.id=production.id_good)
GROUP BY production.id_good,good.name HAVING SUM(production.amount)>100;

3. Выбор товаров, купленных складом.
SELECT warehouse.id,good.name,warehouse.amount,warehouse.cost FROM
warehouse
INNER JOIN good ON (good.id=warehouse.id_good)
WHERE (warehouse.operation=0);

4.Имя Фамилия солдата, его звание и сословие.
WITH citizen_class AS 
(SELECT citizen.id,citizen.name,citizen.surname,class.name AS class FROM citizen LEFT JOIN class ON(class.id=citizen.id_class))
SELECT citizen_class.name,citizen_class.surname,citizen_class.class,soldier_rank.name FROM soldier
LEFT JOIN citizen_class ON (citizen_class.id=soldier.id_citizen)
LEFT JOIN soldier_rank ON (soldier_rank.id=soldier._rank);

Выводы:
В ходе выполнения лабораторной работы были продемонстрированы навыки использования таких конструкций SQL как: INNER/OUTER JOIN, WITH, GROUP BY, ORDER BY, HAVING, WHERE. Не было использована только конструкция UNION поскольку наша БД не имеет таблиц с идентичными полями.
