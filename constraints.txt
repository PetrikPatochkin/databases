citizen (
	1.1 id		первичный ключ,		
	1.1 name		ненулевой,
	1.1 surname		ненулевой,
	1.1 birthday	date,		
	1.3 id_class	внешний ключ на class,		
	1.3 id_region	внешний ключ на region
);

CREATE TABLE region (
	1.1 id		первичный ключ,
	1.1 name	ненулевой,
	1.1 territory	ненулевой, больше нуля	
);

CREATE  TABLE manufacture (
	1.1 id  	первичный ключ,		
	name		,	
	1.3 id_region	внешний ключ на region		
);

CREATE TABLE production (
	1.1 id		первичный ключ,	
	1.3 id_manufacture	внешний ключ на manufacture,	
	1.3 id_good		внешний ключ на good,		
	1.1 amount		больше нуля,		
	_date		
);

CREATE TABLE class (
	1.1 id		первичный ключ,
	name		,	
	tax			
);

CREATE TABLE worker (
	1.3 id_citizen	внешний ключ на citizen,
	1.3 id_manufacture	внешний ключ на manufacture,
	1.1 salary		больше нуля,		
	1.1 hour		больше нуля			
);

CREATE TABLE army_sub (
	1.1 id		первичный ключ,		
	1.1 id_father	ненулевой больше нуля,		
	name				
);

CREATE TABLE soldier (
	1.3 id_citizen	внешний ключ на citien,		
	1.3 id_sub		внешний ключ на army_sub,	
	1.3 _rank		внешний ключ на soldier_rank		
);

CREATE TABLE soldier_rank (
	1.1 id		первичный ключ,		
	name		,	
	1.1 salary		больше нуля		
);

CREATE TABLE good (
	1.1 id		первичный ключ,		
	name		varchar(20),	
	1.1 prod_price	больше нуля,		
	1.1 sale_price	больше нуля,		
	1.1 weight	больше нуля,
	1.2 sale_price > prod_price	
);

CREATE TABLE event (
	1.1 id		первичный ключ,		
	_date		,		
	type		,	
	1.1 description	ненулевой			
);

CREATE TABLE warehouse (
	1.1 id		первичный ключ,
	1.1 operation	только 0 или 1,		
	1.3 id_good		внешний ключ на good,		
	1.1 amount		больше нуля,		
	1.1 cost		больше нуля		
);

CREATE TABLE tax (
	1.1 id		первичный ключ,		
	1.1 id_citizen	внешний ключ на citizen,			
	_date		,		
	1.1 sum_tax		больше нуля		
);
