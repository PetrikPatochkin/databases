INSERT INTO region(name,territory) VALUES ('Ukraine','-55.4'),('Poland','0'),('Slovenia','0'),('Hungary','-140');

INSERT INTO citizen(name, surname, birthday, id_class, id_region) VALUES ('Ivan','Serov','1245-12-25','5','1'),('Vasil','','1298-06-01','3','1');

INSERT INTO event(_date, type) VALUES ('1299-12-12', 'war'),('1305-10-05', 'goverment');

INSERT INTO manufacture(name, id_region) VALUES ('Farm #1','1'),('Farm #2','5'),('Gold Mine #1','6');

INSERT INTO good(name, prod_price, sale_price,weight) VALUES ('Bread','1','0.5','0.5'),('Gold','-3','20','19');

INSERT INTO production(id_manufacture, id_good, amount, _date) VALUES ('5','0','500','1300-05-05'),('2','1','0','1300-05-10'),('3','0','250','1300-08-05');

INSERT INTO worker(id_citizen, id_manufacture, salary, hour) VALUES ('0','1','5','50'),('5','2','-15','40'),('1','3','5','0');

INSERT INTO warehouse(operation, id_good, amount, cost) VALUES ('2','1','-50','80'),('1','1','50','0');

INSERT INTO tax(id_citizen, _date, sum_tax) VALUES ('5','1299-05-05','0'),('0','1299-05-06','10');

INSERT INTO army_sub(id_father, name) VALUES ('0','Army#1'),('abc','Troop#1'),('-1','Troop#2');

INSERT INTO soldier_rank(name, salary) VALUES ('General','50'),('Captain','0'),('Soldier','-2');

INSERT INTO soldier(id_citizen, id_sub, _rank) VALUES ('0','2','3'),('4','3','0');

