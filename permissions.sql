CREATE ROLE minister WiTH SUPERUSER CREATEROLE CREATEDB LOGIN ENCRYPTED PASSWORD '123';
CREATE ROLE admin WiTH CREATEROLE LOGIN ENCRYPTED PASSWORD '123';
CREATE ROLE dbcitizen WiTH LOGIN ENCRYPTED PASSWORD '123';
GRANT CONNECT ON DATABASE igor_db TO admin;
GRANT CONNECT ON DATABASE igor_db TO dbcitizen;
GRANT SELECT,INSERT,UPDATE ON ALL TABLES IN SCHEMA public TO admin;
GRANT SELECT(name,surname,birthday,id_class,id_region)ON citizen TO dbcitizen;
GRANT SELECT(name,territory)ON region TO dbcitizen;
GRANT SELECT(name,id_region)ON manufacture TO dbcitizen;
GRANT SELECT(id_manufacture,id_good,amount,_date)ON production TO dbcitizen;
GRANT SELECT(name)ON class TO dbcitizen;
GRANT SELECT(id_citizen,id_manufacture)ON worker TO dbcitizen;
GRANT SELECT(id,id_father,name)ON army_sub TO dbcitizen;
GRANT SELECT(id_citizen,id_sub,_rank)ON soldier TO dbcitizen;
GRANT SELECT(name)ON soldier_rank TO dbcitizen;
GRANT SELECT(name,sale_price,weight)ON good TO dbcitizen;
GRANT SELECT(_date,description)ON event TO dbcitizen;
