CREATE TABLE citizen (
	id		serial PRIMARY KEY,	--1.1
	name		varchar(20) NOT NULL,	--1.1
	surname		varchar(20) NOT NULL,	--1.1
	birthday	date,		
	id_class	int REFERENCES class(id),	--1.2	
	id_region	int REFERENCES region(id)	--1.2
);

CREATE TABLE region (
	id		serial PRIMARY KEY,	--1.1
	name		varchar(20) NOT NULL,	--1.1
	territory	float NOT NULL CHECK (territory > 0)	--1.1
);

CREATE  TABLE manufacture (
	id  		serial PRIMARY KEY, --1.1
	name		varchar(20),	
	id_region	int CHECK (id_region > 0)	--1.1	
);

CREATE TABLE production (
	id		serial PRIMARY KEY,  --1.1	
	id_manufacture	int REFERENCES manufacture(id), --1.2	
	id_good		int REFERENCES good(id),	--1.2	
	amount		int CHECK (amount > 0),		--1.1
	_date		date		
);

CREATE TABLE class (
	id		serial PRIMARY KEY, --1.1
	name		varchar(20),	
	tax		float	
);

CREATE TABLE worker (
	id_citizen	int REFERENCES citizen(id),		--1.2
	id_manufacture	int REFERENCES manufacture(id),		--1.2
	salary		float CHECK (salary > 0),		--1.1
	hour		int CHECK (hour > 0)			--1.1
);

CREATE TABLE army_sub (
	id		serial PRIMARY KEY,		--1.1
	id_father	int NOT NULL CHECK (id_father >= 0), --1.1		
	name		varchar(20)		
);

CREATE TABLE soldier (
	id_citizen	serial PRIMARY KEY,		--1.1
	id_sub		int REFERENCES army_sub(id),	--1.2
	_rank		int REFERENCES soldier_rank(id)	--1.2	
);

CREATE TABLE soldier_rank (
	id		serial PRIMARY KEY,	--1.1	
	name		varchar(20),	
	salary		float CHECK (salary > 0) --1.1	
);

CREATE TABLE good (
	id		serial PRIMARY KEY,	--1.1	
	name		varchar(20),	
	prod_price	float CHECK (prod_price > 0),	--1.1	
	sale_price	float CHECK (sale_price > 0),	--1.1	
	weight		float CHECK (weight > 0), --1.1
	CHECK (sale_price > prod_price)	--1.2
);

CREATE TABLE event (
	id		serial PRIMARY KEY, --1.1		
	_date		date,		
	type		varchar(20),	
	description	text NOT NULL	--1.1		
);

CREATE TABLE warehouse (
	id		serial PRIMARY KEY, --1.1
	operation	smallint NOT NULL CHECK (operation = 0 OR operation = 1), --1.1
	id_good		int REFERENCES good(id), --1.2
	amount		int NOT NULL CHECK (amount > 0), --1.1		
	cost		float NOT NULL CHECK (cost > 0)	 --1.1	
);

CREATE TABLE tax (
	id		serial PRIMARY KEY,		--1.1
	id_citizen	int REFERENCES citizen(id),	--1.2		
	_date		date,		
	sum_tax		float CHECK (sum_tax > 0)	--1.1
);
