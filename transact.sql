1.SELECT citizen.id,citizen.name,citizen.surname,citizen.birthday,
class.name AS class,region.name AS region FROM citizen 
LEFT JOIN class ON (class.id=citizen.id_class)
LEFT JOIN region ON (region.id=citizen.id_region)
ORDER BY citizen.birthday ASC;

2.SELECT production.id_good,good.name,SUM(production.amount) FROM production
INNER JOIN good ON (good.id=production.id_good)
GROUP BY production.id_good,good.name HAVING SUM(production.amount)>100;

3.SELECT warehouse.id,good.name,warehouse.amount,warehouse.cost FROM
warehouse
INNER JOIN good ON (good.id=warehouse.id_good)
WHERE (warehouse.operation=0);

4.WITH citizen_class AS 
(SELECT citizen.id,citizen.name,citizen.surname,class.name AS class FROM citizen LEFT JOIN class ON(class.id=citizen.id_class))
SELECT citizen_class.name,citizen_class.surname,citizen_class.class,soldier_rank.name FROM soldier
LEFT JOIN citizen_class ON (citizen_class.id=soldier.id_citizen)
LEFT JOIN soldier_rank ON (soldier_rank.id=soldier._rank);
