CREATE OR REPLACE FUNCTION trig_good_funk() RETURNS trigger AS $trig_good$
 DECLARE
 BEGIN
  IF (NEW.sale_price<=NEW.prod_price) THEN 
        RAISE EXCEPTION 'unfair sale price';
  END IF;
  IF (NEW.weight <=0) THEN
        RAISE EXCEPTION 'wrong weight';
  END IF;
  IF (NEW.prod_price <=0) THEN
        RAISE EXCEPTION 'wrong prod price';
  END IF;
  IF (NEW.sale_price <=0) THEN
        RAISE EXCEPTION 'wrong sale price';
  END IF;
  RETURN NEW;
 END;
$trig_good$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_good BEFORE INSERT OR UPDATE ON good FOR each ROW EXECUTE PROCEDURE trig_good_funk();



CREATE OR REPLACE FUNCTION trig_army_del() RETURNS trigger AS $trig_army_sub$
 DECLARE
 BEGIN
   UPDATE soldier SET id_sub = OLD.id_father WHERE id_sub = OLD.id;
   UPDATE army_sub SET id_father = OLD.id_father WHERE id_father = OLD.id;
 RETURN OLD; 
END;
$trig_army_sub$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_army_sub BEFORE DELETE ON army_sub FOR each ROW EXECUTE PROCEDURE trig_army_del();


CREATE TABLE citizen_reg(
        user_name               varchar NOT NULL,
        date_edit               timestamp NOT NULL,
        operation               varchar NOT NULL,                                                                                       
        id_citizen              int NOT NULL,
        name                    varchar NOT NULL,           
        surname                 varchar NOT NULL,                  
        birthday                date NOT NULL,           
        id_class             	int NOT NULL,
        id_region               int NOT NULL
);

CREATE OR REPLACE FUNCTION trig_citizen_reg_funk() RETURNS trigger AS $trig_citizen_reg$
 DECLARE
 BEGIN
   IF (TG_OP = 'DELETE') THEN
          INSERT INTO citizen_reg SELECT user, now(), TG_OP, OLD.id, OLD.name, 
          OLD.surname, OLD.birthday, OLD.id_class,OLD.id_region;
          RETURN OLD;
   ELSE IF (TG_OP = 'INSERT' or TG_OP = 'UPDATE') THEN
          INSERT INTO citizen_reg SELECT user, now(), TG_OP, NEW.id, NEW.name, 
          NEW.surname, NEW.birthday, NEW.id_class,NEW.id_region;
          RETURN NEW;
   END IF;
END IF;
 END;
$trig_citizen_reg$ LANGUAGE  plpgsql;
CREATE TRIGGER trig_citizen_reg AFTER INSERT OR UPDATE OR DELETE ON citizen FOR each ROW EXECUTE PROCEDURE trig_citizen_reg_funk();
