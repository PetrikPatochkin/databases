CREATE VIEW soldier_info AS SELECT b.id,b.name,b.surname,b.birthday,a.id_sub,a._rank FROM citizen b, soldier a WHERE b.id=a.id_citizen;


CREATE FUNCTION insert_update_delete_view() RETURNS trigger AS $insert_update_delete$
BEGIN
        IF TG_OP='INSERT' THEN
        INSERT INTO citizen(id,name, surname, birthday,id_class,id_region) VALUES (NEW.id,NEW.name, NEW.surname, NEW.birthday,3,1);
        INSERT INTO soldier(id_citizen,id_sub, _rank) VALUES
                        (NEW.id,NEW.id_sub, NEW._rank);
        RETURN NEW;

        ELSIF TG_OP='UPDATE' THEN
        UPDATE citizen SET id=NEW.id,name=NEW.name,surname=NEW.surname, birthday=NEW.birthday WHERE id=OLD.id;
        UPDATE soldier SET id_citizen=NEW.id, id_sub=NEW.id_sub, _rank=NEW._rank
                WHERE id_citizen=OLD.id;
        RETURN NEW;

        ELSIF TG_OP='DELETE' THEN
                DELETE FROM soldier WHERE id_citizen=OLD.id;
                DELETE FROM citizen WHERE id=OLD.id;
        RETURN OLD;
        END IF;
END;
$insert_update_delete$ LANGUAGE plpgsql;

CREATE TRIGGER insert_update_delete
INSTEAD OF INSERT OR UPDATE OR DELETE ON soldier_info
FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_view();



CREATE RULE soldier_insert AS ON INSERT TO soldier_info DO INSTEAD(
	INSERT INTO citizen(id,name, surname, birthday,id_class,id_region) VALUES (NEW.id,NEW.name, NEW.surname, NEW.birthday,3,1);
        INSERT INTO soldier(id_citizen,id_sub, _rank) VALUES
                        (NEW.id,NEW.id_sub, NEW._rank);
);
CREATE RULE soldier_update AS ON UPDATE TO soldier_info DO INSTEAD(
	UPDATE citizen SET id=NEW.id,name=NEW.name,surname=NEW.surname, birthday=NEW.birthday WHERE id=OLD.id;
        UPDATE soldier SET id_citizen=NEW.id, id_sub=NEW.id_sub, _rank=NEW._rank
                WHERE id_citizen=OLD.id;
);

CREATE RULE soldier_delete AS ON DELETE TO soldier_info DO INSTEAD(
	DELETE FROM soldier WHERE id_citizen=OLD.id;
        DELETE FROM citizen WHERE id=OLD.id;
);



SELECT * FROM soldier_info;
INSERT INTO soldier_info (id,name,surname,birthday,id_sub,_rank)VALUES(5,'Igor','Tsih','1245-12-12',2,3);
UPDATE soldier_info SET id_sub=5 WHERE id=5;
DELETE FROM soldier_info WHERE id=5;

