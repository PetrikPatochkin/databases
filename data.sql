INSERT INTO class(name,tax) VALUES ('slave','0'),('worker','5.6'),('noble','10');


INSERT INTO region(name,territory) VALUES ('Ukraine','55.4'),('Poland','74.95'),('Slovenia','40.5'),('Hungary','140');


INSERT INTO citizen(name, surname, birthday, id_class, id_region) VALUES ('Ivan','Serov','1245-12-25','1','1'),('Vasil','Capalov','1298-06-02','3','1');


INSERT INTO event(_date, type, description) VALUES ('1299-12-12', 'war', 'war has begun with neighbour'),('1305-10-05', 'goverment', 'new king Victor');


INSERT INTO manufacture(name, id_region) VALUES ('Farm #1','1'),('Farm #2','1'),('Gold Mine #1','2');


INSERT INTO good(name, prod_price, sale_price,weight) VALUES ('Bread','1','1.5','0.5'),('Gold','3','20','19');


INSERT INTO production(id_manufacture, id_good, amount, _date) VALUES ('1','1','500','1300-05-05'),('2','1','1000','1300-05-10'),('3','2','250','1300-08-05');


INSERT INTO worker(id_citizen, id_manufacture, salary, hour) VALUES ('1','1','5','50'),('2','2','15','40'),('1','3','5','50');


INSERT INTO warehouse(operation, id_good, amount, cost) VALUES ('0','1','50','80'),('1','1','50','100');


INSERT INTO tax(id_citizen, _date, sum_tax) VALUES ('3','1299-05-05','5'),('4','1299-05-06','10');


INSERT INTO army_sub(id_father, name) VALUES ('0','Army#1'),('1','Troop#1'),('1','Troop#2');


INSERT INTO soldier_rank(name, salary) VALUES ('General','50'),('Captain','10'),('Soldier','2');


INSERT INTO soldier(id_citizen, id_sub, _rank) VALUES ('3','2','3'),('4','3','3');

